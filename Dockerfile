FROM openjdk:8-jdk-alpine

ENV PATH="/usr/local/sbt/bin:$PATH"

# hadolint ignore=DL4006
RUN apk update && \
    apk add --no-cache --virtual=.build-dependencies wget=1.20.3-r0 ca-certificates=20190108-r0 bash=4.4.19-r1 && \
    mkdir -p "/app/" && \
    mkdir -p "/usr/local/sbt" && \
    wget -qO - "https://piccolo.link/sbt-1.3.10.tgz" | tar xz -C ./usr/local/sbt --strip-components=1 && \
    sbt sbtVersion